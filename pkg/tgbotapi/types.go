package tgbotapi

type Update struct {
	UpdateID int32    `json:"update_id"`
	Message  *Message `json:"message"`
}

type User struct {
	Id        int32  `json:"id"`
	IsBot     bool   `json:"is_bot"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Username  string `json:"username"`
}

type Chat struct {
	ID   int32  `json:"id"`
	Type string `json:"type"`
}

type Message struct {
	MessageID int32 `json:"message_id"`
	From      *User `json:"from"`
	Date      int32 `json:"date"`
}
