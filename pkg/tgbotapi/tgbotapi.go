package tgbotapi

type BotSettings struct {
	ServerURI      string
	APIToken       string
	UpdatesOffset  int32
	UpdatesLimit   int32
	AllowedUpdates []string
}

type BotAPIWrapper struct {
	Settings *BotSettings
	// ...
}

func NewBotAPIWrapper(config *BotConfig) *BotAPIWrapper {
	// ...
	return nil
}

func GetBotSettings() (*BotSettings, error) {
	return nil, nil
}
