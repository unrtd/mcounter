package tgbotapi

import (
	"encoding/json"
	"fmt"
)

func (bot *BotAPIWrapper) GetUpdates() ([]Update, error) {
	endpoint := "/getUpdates"
	request := struct {
		Offset         int32
		Limit          int32
		Timeout        int32
		AllowedUpdates []string
	}{
		Offset:         bot.Settings.UpdatesOffset,
		Limit:          bot.Settings.UpdatesLimit,
		Timeout:        0,
		AllowedUpdates: bot.Settings.AllowedUpdates,
	}

	requestData, err := json.Marshal(request)
	if err != nil {
		return nil, fmt.Errorf("getting updates error: %s (check the bot settings)", err.Error())
	}

	responseData, err := bot.sendRequest(endpoint, requestData)
	var response []Update
	if err := json.Unmarshal(responseData, &response); err != nil {
		return nil, fmt.Errorf("unmarshal updates data error: %s", err.Error())
	}

	return response, nil
}
